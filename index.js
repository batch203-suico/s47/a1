

// [SECTION] Document Object Model
  // allow us to be able to access the or modify the properties of the html element in a webpage.
  // it is a standard on how to get, change, add, or delete HTML elements.
  // we will focus on using DOM for managing forms.
  // For selecting HTML elements will be using the document.querySelector

  // Syntax: document.querySelector("htmlElement")
      // "document" refers to the whole page 
      // ".querySelector" is used to select a specific object (HTML elements) from the document (webpage).
      // The query selector function takes a string input that is formatted like a "CSS selector" when applying the styles.

  // Alternatively, we can use the getElement functions to retrieve the elements.
    // document.getElementById("txt-first-name");

  // However, using these functions require us to identify beforehand how we get the elements. With querySelector, we can be flexible in how to retrieve the elements.

  const txtFirstName = document.querySelector("#txt-first-name");
  const txtLastName = document.querySelector("#txt-last-name");
  const spanFullName = document.querySelector("#span-full-name");
  const txtColor = document.querySelector("#text-color");

  // [SECTION] Event Listeners
    //  Whenever the user interacts with a web page, this action is considered as an event.
    // Working with events is large part of creating interactivity in a webpage.
    // To perform an action when an event triggers, you first need to listen to it.
    
    // The method use is "addEventListener" that takes two arguments:
      //  A string identifying an event;
      //  and a function that the listener will execute once a "specified event" is triggered.
      // WHen an event occurs, an "event object" is passed to the function argument as the first parameter
    txtFirstName.addEventListener("keyup", (event) => {
         console.log(event.target); // contains the element
         console.log(event.target.value); // contains the actual value
         });

    txtFirstName.addEventListener("keyup", ()  => {

      //  The "innerHTML" property sets or returns the HTML content (innerHTML) of an element (div, span, etc.) .
      // The ".value" property sets or returns the value of an attribute. (form control elements: input, select, etc.)
        spanFullName.innerHTML = `${txtFirstName.value}`;
    });

    // Creating Multiple events that will trigger in same function.
    const fullName = () => {
      spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`;
      //document.getElementById("span-full-name").style.color = "blue";
    
    }

    txtColor.addEventListener("click", () => {
      spanFullName.style.color = txtColor.value;
    });

    txtFirstName.addEventListener("keyup", fullName);
    txtLastName.addEventListener("keyup", fullName);